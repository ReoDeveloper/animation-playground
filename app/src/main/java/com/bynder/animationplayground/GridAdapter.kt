package com.bynder.animationplayground

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_media.view.*

class GridAdapter(var items: List<GridItem>): RecyclerView.Adapter<GridAdapter.MediaViewHolder>() {

    fun clearItems() {
        items = emptyList()
        notifyDataSetChanged()
    }

    fun addItems(more: List<GridItem>) {
        items = ArrayList(items).apply { addAll(more) }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MediaViewHolder =
        MediaViewHolder(parent.inflate(R.layout.item_media))

    override fun onBindViewHolder(holder: MediaViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount() = items.size

    inner class MediaViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: GridItem) =
            with(itemView) {
                img_media.loadUrl(item.url)
            }
    }
}